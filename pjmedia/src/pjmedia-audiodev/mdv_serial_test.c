#include <pjmedia-audiodev/audiodev_imp.h>
#include <stdio.h>
#include <string.h>
#include "mdv_serial.h"

#define FRAMES 45

FILE *fp;
int count;

void write_to_file(int fd, unsigned char *buf) {
  fwrite(buf, 1, 16, fp);
  fflush(fp);
  count++;
  /*
  if (count == FRAMES) {
    fclose(fp);
    printf("closed\n");
  }
  */
}

void write_to_both(int fd, unsigned char *buf)
{
    write_to_file(fd, buf);
    mdv_serial_write(fd, buf);
}

void read_from_file(int fd, unsigned char *buf) {
    static FILE *f;
    static int cnt = 0;

    printf(".");
    fflush(stdout);

    if (!f) {
      f = fopen("../../../pj1.mdv", "rb");
      if (!f) {
          puts("Error opening file");
          return;
      }
    }

    if (fread(buf, 16, 1, f) == 1) {
      mdv_serial_write(fd, buf);
      cnt++;
    } else {
      printf("EOF at frame %d\n", cnt);
      mdv_serial_write(fd, buf);
    }
}

void play(int fd) {
    FILE *f;
    unsigned char buffer[32];
    unsigned char dummy[16];
    unsigned char *tmp;

    f = fopen("../../../pj1.mdv", "rb");
    if (!f) {
        puts("Error opening file");
        return;
    }

    while (fread(buffer, 32, 1, f) == 1) {
        tmp = &buffer[0];

        mdv_serial_read(fd, dummy);
        mdv_serial_write(fd, tmp);

        tmp += 16;
        mdv_serial_read(fd, dummy);
        mdv_serial_write(fd, tmp);
    }
    
    while(1) {
    }
}

void play_from_file(int fd) 
{
    FILE *f;
    char buf[16];
    int cnt = 0;

    f = fopen("1.mdv", "rb");
    if (!f) {
        puts("Error opening file");
        return;
    }

    while (fread(buf, 16, 1, f) == 1) {
      mdv_serial_write(fd, buf);
      cnt++;
    } 

    printf("EOF at frame %d\n", cnt);
}
#if 0
int main()
{
    char s[10];
    int fd = mdv_serial_init(MDV_DEVICE_FILE);
    if (fd < 0)
      return 1;
    play_from_file(fd);
    puts("Press ENTER to quit");
    fgets(s, sizeof(s), stdin);
    return 0;
}
#else
int main (int argc, char** argv) {
  int fd;
  count = 0;
  unsigned char buf[16];

  if (argc != 2) {
    puts("Usage: mdv_serial_test cmd");
    puts("where CMD:");
    puts("  rec");
    puts("  play");
    return 0;
  }

  puts("Serial init..");
  fd = mdv_serial_init(MDV_DEVICE_FILE);

  if (strcmp(argv[1], "rec")==0) {
    fp = fopen("1.mdv", "wb");
    if (!fp) {
      puts("Error opening file");
      return 1;
    }
    puts("Recording...");
    mdv_serial_loop(fd, buf, write_to_both);
  } else if (strcmp(argv[1], "play")==0) {
    puts("Reading...");
    mdv_serial_loop(fd, buf, read_from_file);
  } else if (strcmp(argv[1], "play32")==0) {
    puts("Reading...");
    play(fd);
  } else {
    puts("Error: invalid cmd");
  }
#if 0
  mdv_serial_loop(fd, buf, write_to_file);
  mdv_serial_loop(fd, buf, mdv_serial_write);
#endif
  return 0;
}
#endif

