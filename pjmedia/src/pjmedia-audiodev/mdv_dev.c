/* $Id$ */
/* 
 * Copyright (C) 2008-2011 Teluu Inc. (http://www.teluu.com)
 * Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#include <pjmedia-audiodev/audiodev_imp.h>
#include <pj/assert.h>
#include <pj/errno.h>
#include <pj/log.h>
#include <pj/os.h>
#include <pj/string.h>
#include <pj/unicode.h>

#define HAVE_MDV	1

#if defined(HAVE_MDV) && HAVE_MDV
#include "mdv_serial.h"
#endif

#if PJMEDIA_AUDIO_DEV_HAS_MDV

#define THIS_FILE		"mdv_dev.c"

#define DEVICE_INDEX		0

#define FORMAT_ID		PJMEDIA_FORMAT_MDV

#if defined(HAVE_MDV) && HAVE_MDV
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#endif

/* MDV factory */
struct mdv_factory
{
	pjmedia_aud_dev_factory	 base;
	pj_pool_t		*base_pool;
	pj_pool_t		*pool;
	pj_pool_factory		*pf;
	pjmedia_aud_dev_info	 dev_info;
};


/* Sound stream. */
struct mdv_stream
{
	pjmedia_aud_stream   base;		    /**< Base stream	       */
	pjmedia_aud_param    param;		    /**< Settings	       */
	pj_pool_t           *pool;              /**< Memory pool.          */

	unsigned	     bytes_per_frame;   /**< Bytes per frame       */
	unsigned	     xfrm_size;    	    /**< Total ext frm size    */

	void                *user_data;         /**< Application data.     */

	pjmedia_aud_play_cb  play_cb;           /**< Playback callback.    */
	pj_timestamp	     play_ts;
	pj_thread_t	    *play_thread;
	pjmedia_frame_ext   *play_xfrm;	    /**< Extended frame buffer */

	pjmedia_aud_rec_cb   rec_cb;            /**< Capture callback.     */
	pj_timestamp	     rec_ts;
	pj_thread_t	    *rec_thread;
	pjmedia_frame_ext   *rec_xfrm;	    /**< Extended frame buffer */

	pj_thread_t         *play_rec_thread;
	pj_bool_t	     thread_quit_flag;

	pjmedia_format_id    fmt_id;	    /**< Frame format	       */

	int fd; /** serial handler **/
};


/* Prototypes */
static pj_status_t factory_init(pjmedia_aud_dev_factory *f);
static pj_status_t factory_destroy(pjmedia_aud_dev_factory *f);
static pj_status_t factory_refresh(pjmedia_aud_dev_factory *f);
static unsigned    factory_get_dev_count(pjmedia_aud_dev_factory *f);
static pj_status_t factory_get_dev_info(pjmedia_aud_dev_factory *f, 
		unsigned index,
		pjmedia_aud_dev_info *info);
static pj_status_t factory_default_param(pjmedia_aud_dev_factory *f,
		unsigned index,
		pjmedia_aud_param *param);
static pj_status_t factory_create_stream(pjmedia_aud_dev_factory *f,
		const pjmedia_aud_param *param,
		pjmedia_aud_rec_cb rec_cb,
		pjmedia_aud_play_cb play_cb,
		void *user_data,
		pjmedia_aud_stream **p_aud_strm);

static pj_status_t stream_get_param(pjmedia_aud_stream *strm,
		pjmedia_aud_param *param);
static pj_status_t stream_get_cap(pjmedia_aud_stream *strm,
		pjmedia_aud_dev_cap cap,
		void *value);
static pj_status_t stream_set_cap(pjmedia_aud_stream *strm,
		pjmedia_aud_dev_cap cap,
		const void *value);
static pj_status_t stream_start(pjmedia_aud_stream *strm);
static pj_status_t stream_stop(pjmedia_aud_stream *strm);
static pj_status_t stream_destroy(pjmedia_aud_stream *strm);


/* Operations */
static pjmedia_aud_dev_factory_op factory_op =
{
		&factory_init,
		&factory_destroy,
		&factory_get_dev_count,
		&factory_get_dev_info,
		&factory_default_param,
		&factory_create_stream,
		&factory_refresh
};

static pjmedia_aud_stream_op stream_op = 
{
		&stream_get_param,
		&stream_get_cap,
		&stream_set_cap,
		&stream_start,
		&stream_stop,
		&stream_destroy
};


/****************************************************************************
 * Factory operations
 */
/*
 * Init MDV audio driver.
 */
pjmedia_aud_dev_factory* pjmedia_mdv_factory(pj_pool_factory *pf)
{
	struct mdv_factory *f;
	pj_pool_t *pool;

	pool = pj_pool_create(pf, "MDV base", 1000, 1000, NULL);
	f = PJ_POOL_ZALLOC_T(pool, struct mdv_factory);
	f->pf = pf;
	f->base_pool = pool;
	f->base.op = &factory_op;

	return &f->base;
}

static void build_dev_info(pjmedia_aud_dev_info *wdi)
{
	pj_bzero(wdi, sizeof(*wdi));

	/* Device Name */
	strncpy(wdi->name, "MDV dev", sizeof(wdi->name));
	wdi->name[sizeof(wdi->name)-1] = '\0';
	wdi->default_samples_per_sec = MDV_CLOCK_RATE;
	strcpy(wdi->driver, "MDV");

	wdi->input_count = 1;
	wdi->output_count = 1;
	/* Extended formats */
	wdi->caps |= PJMEDIA_AUD_DEV_CAP_EXT_FORMAT;
	wdi->ext_fmt_cnt = 1;
	pjmedia_format_init_audio(&wdi->ext_fmt[0],
			FORMAT_ID, MDV_CLOCK_RATE, 1, MDV_BITS_PER_SAMPLE,
			MDV_PTIME * 1000, MDV_BITRATE, MDV_BITRATE);
}

/* API: init factory */
static pj_status_t factory_init(pjmedia_aud_dev_factory *f)
{
	pj_status_t ret = factory_refresh(f);
	if (ret != PJ_SUCCESS)
		return ret;

	PJ_LOG(4, (THIS_FILE, "MDV initialized"));
	return PJ_SUCCESS;
}

/* API: refresh the device list */
static pj_status_t factory_refresh(pjmedia_aud_dev_factory *f)
{
	struct mdv_factory *wf = (struct mdv_factory*)f;

	if (wf->pool != NULL) {
		pj_pool_release(wf->pool);
		wf->pool = NULL;
	}

	wf->pool = pj_pool_create(wf->pf, "MDV", 1000, 1000, NULL);

	build_dev_info(&wf->dev_info);

	PJ_LOG(4, (THIS_FILE, "dev_id %d: %s  (in=%d, out=%d)",
			DEVICE_INDEX,
			wf->dev_info.name,
			wf->dev_info.input_count,
			wf->dev_info.output_count));

	return PJ_SUCCESS;
}

/* API: destroy factory */
static pj_status_t factory_destroy(pjmedia_aud_dev_factory *f)
{
	struct mdv_factory *wf = (struct mdv_factory*)f;
	pj_pool_t *pool = wf->base_pool;

	pj_pool_release(wf->pool);
	wf->base_pool = NULL;
	pj_pool_release(pool);

	return PJ_SUCCESS;
}

/* API: get number of devices */
static unsigned factory_get_dev_count(pjmedia_aud_dev_factory *f)
{
	return 1;
}

/* API: get device info */
static pj_status_t factory_get_dev_info(pjmedia_aud_dev_factory *f,
		unsigned index,
		pjmedia_aud_dev_info *info)
{
	struct mdv_factory *wf = (struct mdv_factory*)f;

	PJ_ASSERT_RETURN(index == DEVICE_INDEX, PJMEDIA_EAUD_INVDEV);
	pj_memcpy(info, &wf->dev_info, sizeof(*info));
	return PJ_SUCCESS;
}

/* API: create default device parameter */
static pj_status_t factory_default_param(pjmedia_aud_dev_factory *f,
		unsigned index,
		pjmedia_aud_param *param)
{
	struct mdv_factory *wf = (struct mdv_factory*)f;

	PJ_ASSERT_RETURN(index == DEVICE_INDEX, PJMEDIA_EAUD_INVDEV);

	pj_bzero(param, sizeof(*param));
	param->dir = PJMEDIA_DIR_CAPTURE_PLAYBACK;
	param->rec_id = DEVICE_INDEX;
	param->play_id = DEVICE_INDEX;

	param->clock_rate = wf->dev_info.default_samples_per_sec;
	param->channel_count = 1;
	param->samples_per_frame = wf->dev_info.default_samples_per_sec * MDV_PTIME / 1000;
	param->bits_per_sample = MDV_BITS_PER_SAMPLE;
	param->flags |= PJMEDIA_AUD_DEV_CAP_EXT_FORMAT;
	param->ext_fmt = wf->dev_info.ext_fmt[0];

	return PJ_SUCCESS;
}

/* Get format name */
static const char *get_fmt_name(pj_uint32_t id)
{
	static char name[8];

	pj_memcpy(name, &id, 4);
	name[4] = '\0';
	return name;
}

/* Internal: create MDV player device. */
static pj_status_t init_player_stream(  struct mdv_factory *wf,
		pj_pool_t *pool,
		struct mdv_stream *strm,
		const pjmedia_aud_param *prm)
{
	/* Done setting up play device. */
	PJ_LOG(4, (THIS_FILE,
			" MDV player \"%s\" initialized ("
			"format=%s, clock_rate=%d, "
			"channel_count=%d, samples_per_frame=%d (%dus))",
			wf->dev_info.name,
			get_fmt_name(prm->ext_fmt.id),
			prm->clock_rate, prm->channel_count, prm->samples_per_frame,
			prm->samples_per_frame * 1000000 / prm->clock_rate));

	return PJ_SUCCESS;
}

/* Internal: create Windows Multimedia recorder device */
static pj_status_t init_capture_stream( struct mdv_factory *wf,
		pj_pool_t *pool,
		struct mdv_stream *strm,
		const pjmedia_aud_param *prm)
{
	PJ_ASSERT_RETURN(prm->rec_id == DEVICE_INDEX, PJ_EINVAL);

	/* Done setting up play device. */
	PJ_LOG(4,(THIS_FILE,
			" MDV recorder \"%s\" initialized "
			"(format=%s, clock_rate=%d, "
			"channel_count=%d, samples_per_frame=%d (%dus))",
			wf->dev_info.name,
			get_fmt_name(prm->ext_fmt.id),
			prm->clock_rate, prm->channel_count, prm->samples_per_frame,
			prm->samples_per_frame * 1000000 / prm->clock_rate));

	return PJ_SUCCESS;
}

static int mdv_write(int fd, const void *buf, unsigned len)
{
	//PJ_LOG(5,(THIS_FILE, "mdv_write %d", len));
	mdv_serial_write_n(fd, buf, len/MDV_DATA_LEN);
	return 0;
}

static int mdv_read(int fd, void *buf, unsigned max_len)
{
	mdv_serial_read_n(fd, buf, max_len/MDV_DATA_LEN);
	return max_len;
}

static int mdv_capture_playback_thread(void *arg)
{
	struct mdv_stream *strm = (struct mdv_stream*)arg;
	unsigned char rec_buffer[MDV_BYTES_PER_FRAME];
	unsigned char play_buffer[MDV_BYTES_PER_FRAME];
	int has_play_frame;
	int cap_len;
	pj_status_t status = PJ_SUCCESS;
	pjmedia_frame_ext *xfrm = strm->play_xfrm;
	pjmedia_frame *frame;

	PJ_LOG(4,(THIS_FILE, "MDV wait for header"));
	mdv_serial_wait_for_header(strm->fd, rec_buffer);

	PJ_LOG(4,(THIS_FILE, "MDV capture playback thread started"));

	/*
	 * Loop while not signalled to quit, wait for event objects to be
	 * signalled by MDV capture and play buffer.
	 */
	while (!strm->thread_quit_flag) {
		has_play_frame = 0;

		cap_len = mdv_read(strm->fd, rec_buffer, MDV_DATA_LEN);
		pj_assert(cap_len == MDV_DATA_LEN);

		frame = &xfrm->base;

		xfrm->base.type = PJMEDIA_FRAME_TYPE_EXTENDED;
		xfrm->base.size = strm->bytes_per_frame;
		xfrm->base.buf = NULL;
		xfrm->base.timestamp.u64 = strm->play_ts.u64;
		xfrm->base.bit_info = 0;

		/* Get frame from application. */
		//PJ_LOG(5,(THIS_FILE, "xxx %u play_cb", play_cnt++));
		status = (*strm->play_cb)(strm->user_data, frame);

		if (status != PJ_SUCCESS)
			break;

		/* Codec mode */
		if (frame->type == PJMEDIA_FRAME_TYPE_NONE) {
			/* got no frame */
		} else if (frame->type == PJMEDIA_FRAME_TYPE_EXTENDED) {
			unsigned sz;
			sz = pjmedia_frame_ext_copy_payload(xfrm,
					play_buffer, strm->bytes_per_frame);
			if (sz < strm->bytes_per_frame) {
				pj_bzero((char*)play_buffer+sz, strm->bytes_per_frame - sz);
			}
			//PJ_LOG(5,(THIS_FILE, "Got data %d bytes", sz));

			/* Write to the device. */
			mdv_write(strm->fd, play_buffer, MDV_DATA_LEN);
			has_play_frame = 1;
		} else {
			pj_assert(!"Frame type not supported");
		}

		strm->play_ts.u64 += strm->param.samples_per_frame;

		int cap_len;
		pjmedia_frame_ext *xfrm = strm->rec_xfrm;
		pjmedia_frame *frame;

		cap_len = mdv_read(strm->fd, rec_buffer+MDV_DATA_LEN, MDV_DATA_LEN);
		pj_assert(cap_len == MDV_DATA_LEN);

		frame = &xfrm->base;
		frame->type = PJMEDIA_FRAME_TYPE_EXTENDED;
		frame->buf = NULL;
		frame->size = strm->bytes_per_frame;
		frame->timestamp.u64 = strm->rec_ts.u64;
		frame->bit_info = 0;

		xfrm->samples_cnt = 0;
		xfrm->subframe_cnt = 0;
		pjmedia_frame_ext_append_subframe(xfrm, rec_buffer,
				strm->bytes_per_frame * 8,
				strm->param.samples_per_frame);

		/* Call callback */
		//PJ_LOG(5,(THIS_FILE, "xxx %u rec_cb", rec_cnt++));
		status = (*strm->rec_cb)(strm->user_data, frame);
		if (status != PJ_SUCCESS)
			break;

		/* Increment position. */
		strm->rec_ts.u64 += strm->param.samples_per_frame ;

		if (has_play_frame) {
			mdv_write(strm->fd, play_buffer+MDV_DATA_LEN, MDV_DATA_LEN);
		}
	}

	PJ_LOG(4,(THIS_FILE, "MDV capture playback thread stopping.."));
	return 0;
}

/* API: create stream */
static pj_status_t factory_create_stream(pjmedia_aud_dev_factory *f,
		const pjmedia_aud_param *param,
		pjmedia_aud_rec_cb rec_cb,
		pjmedia_aud_play_cb play_cb,
		void *user_data,
		pjmedia_aud_stream **p_aud_strm)
{
	struct mdv_factory *wf = (struct mdv_factory*)f;
	pj_pool_t *pool;
	struct mdv_stream *strm;
	pj_status_t status;

	if (param->clock_rate != MDV_CLOCK_RATE) {
		PJ_LOG(1,(THIS_FILE, "Error: unsupported clock rate %d", param->clock_rate));
		return PJ_ENOTSUP;
	}
	if (param->channel_count != 1) {
		PJ_LOG(1,(THIS_FILE, "Error: unsupported channel_count %d", param->channel_count));
		return PJ_ENOTSUP;
	}
	if (param->samples_per_frame != MDV_CLOCK_RATE * MDV_PTIME / 1000) {
		PJ_LOG(1,(THIS_FILE, "Error: unsupported samples_per_frame %d", param->samples_per_frame));
		return PJ_ENOTSUP;
	}
	if (param->ext_fmt.id != FORMAT_ID) {
		PJ_LOG(1,(THIS_FILE, "Error: unsupported format %d", param->ext_fmt.id));
		return PJMEDIA_EAUD_BADFORMAT;
	}

	/* Create and Initialize stream descriptor */
	pool = pj_pool_create(wf->pf, "mdv-dev", 1000, 1000, NULL);
	PJ_ASSERT_RETURN(pool != NULL, PJ_ENOMEM);

	strm = PJ_POOL_ZALLOC_T(pool, struct mdv_stream);
	pj_memcpy(&strm->param, param, sizeof(*param));
	strm->pool = pool;
	strm->rec_cb = rec_cb;
	strm->play_cb = play_cb;
	strm->user_data = user_data;
	strm->bytes_per_frame = PJMEDIA_FSZ(param->ext_fmt.det.aud.avg_bps, param->ext_fmt.det.aud.frame_time_usec);
	strm->fd = mdv_serial_init(MDV_DEVICE_FILE);
	if (strm->fd < 0) {
		return pj_get_os_error();
	}

	/* Create player stream */
	if (param->dir & PJMEDIA_DIR_PLAYBACK) {
		status = init_player_stream(wf, strm->pool, strm, param);
		if (status != PJ_SUCCESS) {
			stream_destroy(&strm->base);
			return status;
		}
	}

	/* Create capture stream */
	if (param->dir & PJMEDIA_DIR_CAPTURE) {
		status = init_capture_stream(wf, strm->pool, strm, param);
		if (status != PJ_SUCCESS) {
			stream_destroy(&strm->base);
			return status;
		}
	}

	strm->xfrm_size = sizeof(pjmedia_frame_ext) +
			32 * sizeof(pjmedia_frame_ext_subframe) +
			strm->bytes_per_frame + 4;
	strm->play_xfrm = (pjmedia_frame_ext*) pj_pool_alloc(pool, strm->xfrm_size);
	strm->rec_xfrm = (pjmedia_frame_ext*) pj_pool_alloc(pool, strm->xfrm_size);

	/* Done */
	strm->base.op = &stream_op;
	*p_aud_strm = &strm->base;

	return PJ_SUCCESS;
}

/* API: Get stream info. */
static pj_status_t stream_get_param(pjmedia_aud_stream *s,
		pjmedia_aud_param *pi)
{
	struct mdv_stream *strm = (struct mdv_stream*)s;

	PJ_ASSERT_RETURN(strm && pi, PJ_EINVAL);
	pj_memcpy(pi, &strm->param, sizeof(*pi));

	return PJ_SUCCESS;
}

/* API: get capability */
static pj_status_t stream_get_cap(pjmedia_aud_stream *s,
		pjmedia_aud_dev_cap cap,
		void *pval)
{
	struct mdv_stream *strm = (struct mdv_stream*)s;
	PJ_UNUSED_ARG(strm);
	PJ_ASSERT_RETURN(s && pval, PJ_EINVAL);
	return PJMEDIA_EAUD_INVCAP;
}

/* API: set capability */
static pj_status_t stream_set_cap(pjmedia_aud_stream *s,
		pjmedia_aud_dev_cap cap,
		const void *pval)
{
	struct mdv_stream *strm = (struct mdv_stream*)s;
	PJ_UNUSED_ARG(strm);
	PJ_ASSERT_RETURN(s && pval, PJ_EINVAL);
	return PJMEDIA_EAUD_INVCAP;
}

/* API: Start stream. */
static pj_status_t stream_start(pjmedia_aud_stream *strm)
{
	struct mdv_stream *stream = (struct mdv_stream*)strm;
	pj_status_t status;

	stream->thread_quit_flag = PJ_FALSE;

	/* Use 1 thread for both rec and play, to synchronize playback clock to rec clock */
	pj_assert(stream->play_rec_thread == NULL);
	status = pj_thread_create(stream->pool, "mdv%p", &mdv_capture_playback_thread, strm, 0, 0,
			&stream->play_rec_thread);
	if (status != PJ_SUCCESS) {
		pj_perror(1, THIS_FILE, status, "Error creating playback_capture thread");
		return status;
	}

	return PJ_SUCCESS;
}

/* API: Stop stream. */
static pj_status_t stream_stop(pjmedia_aud_stream *strm)
{
	struct mdv_stream *stream = (struct mdv_stream*)strm;

	stream->thread_quit_flag = PJ_TRUE;

	if (stream->play_rec_thread) {
		pj_thread_join(stream->play_rec_thread);
		pj_thread_destroy(stream->play_rec_thread);
		stream->play_rec_thread = NULL;
	}
	if (stream->play_thread) {
		pj_thread_join(stream->play_thread);
		pj_thread_destroy(stream->play_thread);
		stream->play_thread = NULL;
	}

	if (stream->rec_thread) {
		pj_thread_join(stream->rec_thread);
		pj_thread_destroy(stream->rec_thread);
		stream->rec_thread = NULL;
	}

	return PJ_SUCCESS;
}

/* API: Destroy stream. */
static pj_status_t stream_destroy(pjmedia_aud_stream *strm)
{
	struct mdv_stream *stream = (struct mdv_stream*)strm;

	PJ_ASSERT_RETURN(stream != NULL, PJ_EINVAL);

	mdv_serial_destroy(stream->fd);

	stream_stop(strm);

	pj_pool_release(stream->pool);

	return PJ_SUCCESS;
}

#endif	/* PJMEDIA_AUDIO_DEV_HAS_MDV */

