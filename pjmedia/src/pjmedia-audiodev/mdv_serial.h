#pragma once

#include <termios.h>

int mdv_serial_init(const char *dev);
void mdv_serial_destroy(int fd);
void mdv_serial_loop(int fd, unsigned char* buf, void (*read_cb)(int, unsigned char *));
int mdv_serial_read(int fd, unsigned char *buf);
void mdv_serial_write(int fd, const unsigned char* buf);
int mdv_serial_wait_for_header(int fd, unsigned char *buf);
void mdv_serial_read_n(int fd, unsigned char *buf, short n);
void mdv_serial_write_n(int fd, const unsigned char *buf, short n);

