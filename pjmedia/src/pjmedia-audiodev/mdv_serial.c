#include <pjmedia-audiodev/audiodev_imp.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include "mdv_serial.h"

static struct termios tio_original_opts;

int mdv_serial_init(const char *dev)
{
    int fd = -1;
    struct termios tio;
    fd = open(dev, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd < 0) {
         return -1;
    }
    if (ioctl(fd, TIOCEXCL) < 0) {
         return -1;
    }
    if (fcntl(fd, F_SETFL, 0) < 0) {
         return -1;
    }
    if (tcgetattr(fd, &tio_original_opts) < 0) {
         return -1;
    }
    tio = tio_original_opts;
    cfmakeraw(&tio);
    cfsetspeed(&tio, MDV_BAUD_RATE);
    tio.c_cc[VMIN] = 16;
    tio.c_cc[VTIME] = 0;
    tio.c_cflag |= (CS8 | CLOCAL | CREAD);
    tio.c_iflag = 0;
    tio.c_oflag = 0;
    tio.c_lflag = 0;

    // check if we need to use ioctl here, especially on mac
    // ioctl(fd, IOSSIOSPEED, &speed), where speed is speed_t
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &tio);
    return fd;
}

void mdv_serial_destroy(int fd) {
	close(fd);
}

void mdv_serial_write(int fd, const unsigned char *buf) {
    write(fd, buf, MDV_DATA_LEN);
}

void mdv_serial_loop(int fd, unsigned char *buf, void (*read_cb)(int, unsigned char *)) {
    int rc = -1;
    int hc = -1;
    do {
        while(hc < 0) {
            puts("MDV serial wait for header..");
            hc = mdv_serial_wait_for_header(fd, buf);  
            puts("MDV serial found header..");
        }
        rc = read(fd, buf, MDV_DATA_LEN);
        (*read_cb)(fd, buf);
    }
    while(rc == MDV_DATA_LEN && (buf[0] == 0x4C && buf[1] == 0x4E));
    printf("data dropped, with header: %.2x %2.x\n", buf[0], buf[1]);
    mdv_serial_loop(fd, buf, read_cb);
}

int mdv_serial_read(int fd, unsigned char *buf) 
{
    return read(fd, buf, MDV_DATA_LEN);
}

int mdv_serial_wait_for_header(int fd, unsigned char *buf) {
    struct termios tio;
    int i;
    while (1) {
      read(fd, buf, 1);
      if (buf[0] == 0x4C) {
          read(fd, buf, 1);
          if (buf[0] == 0x4E) {
              for (i = 2; i < MDV_DATA_LEN; i++) {
                  read(fd, buf, 1);
              }
              if (tcgetattr(fd, &tio) < 0) {
                  return -1;
              }

              return 0;
          }
      }
    }
    return -1;
}

void mdv_serial_read_n(int fd, unsigned char *buf, short n) {
	short i;

	for(i=0; i<n; i++) {
		mdv_serial_read(fd, buf);
		buf += 16;
	}
}

void mdv_serial_write_n(int fd, const unsigned char *buf, short n) {
	short i;

	for(i=0; i<n; i++) {
		mdv_serial_write(fd, buf);
		buf += 16;
	}
}

