/* $Id: mdv_reader.c 3664 2011-07-19 03:42:28Z nanang $ */
/*
 * Copyright (C) 2008-2011 Teluu Inc. (http://www.teluu.com)
 * Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <pjmedia/mdv_file.h>
#include <pj/assert.h>
#include <pj/errno.h>
#include <pj/file_io.h>
#include <pj/log.h>
#include <pj/pool.h>

#define THIS_FILE           "mdv_file.c"
#define SIGNATURE           PJMEDIA_SIG_CLASS_PORT_AUD('R', 'W')
#define BYTES_PER_SAMPLE    2

struct mdv_reader
{
    pjmedia_port     base;
    pj_oshandle_t    fd;
    pj_timestamp     timestamp;
    char            *buffer;
    pj_size_t        bytes_per_frame;
    pjmedia_frame_ext *xfrm;
};

struct mdv_writer
{
    pjmedia_port     base;
    pj_oshandle_t    fd;
    char            *buffer;
    pj_size_t        bytes_per_frame;
};


static pj_status_t writer_put_frame(pjmedia_port *this_port,
                                 pjmedia_frame *frame);
static pj_status_t writer_get_frame(pjmedia_port *this_port,
                                  pjmedia_frame *frame);
static pj_status_t writer_on_destroy(pjmedia_port *this_port);

static pj_status_t reader_put_frame(pjmedia_port *this_port,
                                 pjmedia_frame *frame);
static pj_status_t reader_get_frame(pjmedia_port *this_port,
                                  pjmedia_frame *frame);
static pj_status_t reader_on_destroy(pjmedia_port *this_port);


PJ_DEF(pj_status_t) pjmedia_mdv_reader_create(pj_pool_t *pool,
                                              const char *filename,
                                              pjmedia_port **p_port )
{
    struct mdv_reader *port;
    pjmedia_format fmt;
    pj_str_t name = pj_str("mdvreader");
    unsigned xfrm_size;
    pj_status_t status;

    port = PJ_POOL_ZALLOC_T(pool, struct mdv_reader);
    PJ_ASSERT_RETURN(port != NULL, PJ_ENOMEM);

    status = pj_file_open(pool, filename, PJ_O_RDONLY, &port->fd);
    if (status != PJ_SUCCESS) {
        PJ_PERROR(1,(THIS_FILE, status, "Error opening mdv file"));
        return status;
    }

    pjmedia_format_init_audio(&fmt,
                              PJMEDIA_FORMAT_MDV, MDV_CLOCK_RATE, 1, MDV_BITS_PER_SAMPLE,
                              MDV_PTIME * 1000, MDV_BITRATE, MDV_BITRATE);

    pjmedia_port_info_init2(&port->base.info, &name, SIGNATURE,
                            PJMEDIA_DIR_PLAYBACK, &fmt);

    port->bytes_per_frame = fmt.det.aud.max_bps * fmt.det.aud.frame_time_usec / 1000000 / 8;
    port->buffer = (char*)pj_pool_alloc(pool, port->bytes_per_frame);

    xfrm_size = sizeof(pjmedia_frame_ext) +
                      32 * sizeof(pjmedia_frame_ext_subframe) +
                      port->bytes_per_frame + 4;
    port->xfrm = (pjmedia_frame_ext*) pj_pool_alloc(pool, xfrm_size);

    port->base.put_frame = &reader_put_frame;
    port->base.get_frame = &reader_get_frame;
    port->base.on_destroy = &reader_on_destroy;

    *p_port = &port->base;

    return PJ_SUCCESS;
}

static pj_status_t reader_put_frame( pjmedia_port *this_port,
                                  pjmedia_frame *frame)
{
    PJ_UNUSED_ARG(this_port);
    PJ_UNUSED_ARG(frame);

    return PJ_SUCCESS;
}


static pj_status_t reader_get_frame(pjmedia_port *this_port,
                                     pjmedia_frame *frame)
{
    struct mdv_reader *player = (struct mdv_reader*)this_port;
    pjmedia_frame_ext *xfrm;
    //pjmedia_frame *frame;
    pj_ssize_t size;
    pj_status_t status;

    size = player->bytes_per_frame;
    status = pj_file_read(player->fd, player->buffer, &size);
    if (status != PJ_SUCCESS || size != player->bytes_per_frame) {
        frame->type = PJMEDIA_FRAME_TYPE_NONE;
        frame->size = 0;
        frame->buf = NULL;
        return status;
    }

    //frame = &xfrm->base;
    frame->type = PJMEDIA_FRAME_TYPE_EXTENDED;
    frame->buf = NULL;
    frame->size = player->bytes_per_frame;
    frame->timestamp.u64 = player->timestamp.u64;
    frame->bit_info = 0;

    xfrm = (pjmedia_frame_ext*)frame;
    xfrm->samples_cnt = 0;
    xfrm->subframe_cnt = 0;
    pjmedia_frame_ext_append_subframe(xfrm, player->buffer,
                                      player->bytes_per_frame * 8,
                                      player->base.info.fmt.det.aud.clock_rate *
                                          player->base.info.fmt.det.aud.frame_time_usec /
                                          1000000);

    player->timestamp.u64 += PJMEDIA_PIA_SPF(&this_port->info);

    return PJ_SUCCESS;
}


static pj_status_t reader_on_destroy(pjmedia_port *this_port)
{
    struct mdv_reader *player = (struct mdv_reader*)this_port;
    pj_file_close(player->fd);
    player->fd = 0;
    return PJ_SUCCESS;
}


PJ_DEF(pj_status_t) pjmedia_mdv_writer_create(pj_pool_t *pool,
                                              const char *filename,
                                              pjmedia_port **p_port)
{
    struct mdv_writer *rec;
    const pj_str_t name = pj_str("mdvwriter");
    pjmedia_format fmt;
    pj_status_t status;

    rec = PJ_POOL_ZALLOC_T(pool, struct mdv_writer);
    PJ_ASSERT_RETURN(rec != NULL, PJ_ENOMEM);

    pjmedia_format_init_audio(&fmt,
                              PJMEDIA_FORMAT_MDV, MDV_CLOCK_RATE, 1, MDV_BITS_PER_SAMPLE,
                              MDV_PTIME * 1000, MDV_BITRATE, MDV_BITRATE);

    /* Create the rec */
    pjmedia_port_info_init2(&rec->base.info, &name, SIGNATURE, PJMEDIA_DIR_CAPTURE, &fmt);

    status = pj_file_open(pool, filename, PJ_O_WRONLY, &rec->fd);
    if (status != PJ_SUCCESS)
        return status;

    rec->bytes_per_frame = fmt.det.aud.max_bps * fmt.det.aud.frame_time_usec / 1000000 / 8;
    rec->buffer = (char*)pj_pool_alloc(pool, rec->bytes_per_frame);

    rec->base.put_frame = &writer_put_frame;
    rec->base.get_frame = &writer_get_frame;
    rec->base.on_destroy = &writer_on_destroy;

    *p_port = &rec->base;

    return PJ_SUCCESS;
}

static pj_status_t writer_put_frame( pjmedia_port *this_port,
                                     pjmedia_frame *frame)
{
    struct mdv_writer *rec;
    pjmedia_frame_ext *xfrm = (pjmedia_frame_ext*)frame;
    unsigned sz;
    pj_ssize_t size_written;
    pj_status_t status;

    rec = (struct mdv_writer*) this_port;

    if (frame->type == PJMEDIA_FRAME_TYPE_NONE) {
        return PJ_SUCCESS;
    } else if (frame->type != PJMEDIA_FRAME_TYPE_EXTENDED) {
        PJ_LOG(1,(THIS_FILE, "Error: expecting PJMEDIA_FRAME_TYPE_EXTENDED frame"));
        return PJ_EINVAL;
    }
    sz = pjmedia_frame_ext_copy_payload(xfrm, rec->buffer, rec->bytes_per_frame);
    if (sz < rec->bytes_per_frame) {
        pj_bzero((char*)rec->buffer+sz, rec->bytes_per_frame - sz);
    }

    size_written = rec->bytes_per_frame;
    status = pj_file_write(rec->fd, rec->buffer, &size_written);
    if (status != PJ_SUCCESS) {
        PJ_PERROR(1,(THIS_FILE, status, "Error writing file"));
        return status;
    }

    pj_file_flush(rec->fd);
    return PJ_SUCCESS;
}

static pj_status_t writer_get_frame( pjmedia_port *this_port,
                                     pjmedia_frame *frame)
{
    PJ_UNUSED_ARG(this_port);
    frame->size = 0;
    frame->type = PJMEDIA_FRAME_TYPE_NONE;
    return PJ_SUCCESS;
}

static pj_status_t writer_on_destroy(pjmedia_port *this_port)
{
    struct mdv_writer *rec;

    rec = (struct mdv_writer*) this_port;
    pj_file_close(rec->fd);
    rec->fd = 0;
    return PJ_SUCCESS;
}
