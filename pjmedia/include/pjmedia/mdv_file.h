/* $Id: rawfile_port.h 3553 2011-05-05 06:14:19Z nanang $ */
/* 
 * Copyright (C) 2008-2011 Teluu Inc. (http://www.teluu.com)
 * Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */
#ifndef __PJMEDIA_RAWFILE_PORT_H__
#define __PJMEDIA_RAWFILE_PORT_H__

/**
 * @file rawfile_port.h
 * @brief Memory based media playback/capture port
 */
#include <pjmedia/port.h>

PJ_BEGIN_DECL

/**
 * @defgroup PJMEDIA_RAWFILE_PLAYER Memory/Buffer-based Playback Port
 * @ingroup PJMEDIA_PORT
 * @brief Media playback from a fixed size rawfileory buffer
 * @{
 *
 * A rawfileory/buffer based playback port is used to play media from a fixed
 * size buffer. This is useful over @ref PJMEDIA_FILE_PLAY for
 * situation where filesystems are not available in the target system.
 */

PJ_DECL(pj_status_t) pjmedia_mdv_reader_create(pj_pool_t *pool,
					       const char *filename,
					       pjmedia_port **p_port );

/**
 * @}
 */

/**
 * @defgroup PJMEDIA_RAWFILE_CAPTURE Memory/Buffer-based Capture Port
 * @ingroup PJMEDIA_PORT
 * @brief Media capture to fixed size rawfileory buffer
 * @{
 *
 * A rawfileory based capture is used to save media streams to a fixed size
 * buffer. This is useful over @ref PJMEDIA_FILE_REC for
 * situation where filesystems are not available in the target system.
 */

PJ_DECL(pj_status_t) pjmedia_mdv_writer_create(pj_pool_t *pool,
					       const char *filename,
					       pjmedia_port **p_port);


/**
 * @}
 */

PJ_END_DECL

#endif	/* __PJMEDIA_RAWFILE_PORT_H__ */
